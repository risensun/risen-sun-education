import {
  uniqueNamesGenerator,
  adjectives,
  colors,
  animals,
} from 'unique-names-generator';

export const REQUESTS = {
  createSkill: '/users/',
};

export const CONFIG = {};

/* eslint-disable @typescript-eslint/no-var-requires */
/* eslint-disable @typescript-eslint/no-require-imports */
/* eslint-disable no-console */
/**
 * To run script type: npm run script {scriptName}
 * scriptName - same as script file name
 * For example: npm run script migrateProductType
 */

async function bootstrap() {
  console.log(`#### BOOTSTRAP ${process.env.ENV}`);

  const scriptName = process.argv[2];

  const { default: action } = require(`./${scriptName}`);

  try {
    return await action(process.argv[3], process.argv[4], process.argv[5]);
  } catch (exception) {
    console.error(exception);
  }
}

bootstrap()
  .then((result) => {
    console.log('#### DONE\n', result);
    process.exit();
  })
  .catch(console.error);

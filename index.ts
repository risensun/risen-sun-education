import { DistributedStartup } from '@glangeo/pollux/distributed/http';
import { loadAliases, loadEnvironment } from '@glangeo/pollux/utils';

loadAliases();
loadEnvironment();

import { Config } from 'src/Config';
import { ServicesApp } from 'src/services';
import { RestApiApp } from 'src/rest-api/app';
import { App } from './src/App';

const app = new App({
  baseRoute: '/',
  logging: {
    isEnabled: true,
    router: {
      routeAdded: true,
      incomimgRequests: true,
    },
    app: {
      init: true,
    },
    distributed: {
      detachedServiceAdded: true,
      remoteCallReceived: true,
      remoteCallResponded: true,
    },
  },
});

const startup = new DistributedStartup({
  currentAppName: Config.getAppName(),
  apps: [
    {
      app,
      name: 'REST_API_V1',
      port: 3000,
      host: 'http://localhost:3000',
      childApps: [new RestApiApp({ baseRoute: '/api/v1' }), new ServicesApp()],
    },
  ],
});

startup.init().then((app) => app.listen());

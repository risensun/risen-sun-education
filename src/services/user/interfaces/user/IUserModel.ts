import { User } from 'src/common/types/user';
import { UserCreatePayload } from '../../types';

export interface IUserModel {
  create(payload: UserCreatePayload): Promise<User>;

  getById(id: number): Promise<User>;

  getAll(): Promise<User[]>;

  updatePersonalInfo(
    id: User['id'],
    updates: Pick<
      User,
      'firstName' | 'lastName' | 'phone' | 'jobTitle' | 'dateOfBirth'
    >
  ): Promise<boolean>;
}

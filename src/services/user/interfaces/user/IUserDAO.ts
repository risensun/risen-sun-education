import { IEntityDAO } from 'src/common';
import { User } from 'src/common/types/user';
import { UserCreatePayload } from '../../types';

export interface IUserDAO extends IEntityDAO<User> {
  create(payload: UserCreatePayload): Promise<User>;

  getByEmail(email: User['email']): Promise<User>;

  getAll(): Promise<User[]>;

  updatePersonalInfo(
    id: User['id'],
    updates: Pick<
      User,
      'firstName' | 'lastName' | 'phone' | 'jobTitle' | 'dateOfBirth'
    >
  ): Promise<boolean>;
}

import { Registry } from './registry';

export class UserService {
  public async createSuperUser(): Promise<void> {
    try {
      const user = await Registry.getUserModel().create({
        email: 'mglavanar@risensun.agency',
        phone: '+79819887003',
        firstName: 'Makar',
        lastName: 'Glavanar',
        dateOfBirth: new Date('05.09.2000 00:00:00').getTime(),
        jobTitle: 'CEO',
      });

      await Registry.getAuthService().register(user.email, '12345678');

      // eslint-disable-next-line no-console
      console.log('Super user created.');
    } catch (error) {
      // eslint-disable-next-line no-console
      console.log('Super user already exists.');
    }
  }

  /** Aliases */
  public createUser = Registry.getUserModel().create;
  public getUserById = Registry.getUserModel().getById;
  public getUsers = Registry.getUserModel().getAll;
  public updateUserProfileInfo = Registry.getUserModel().updatePersonalInfo;
}

import { NotFoundException, ValidationException } from '@glangeo/pollux';
import { bindMethods, throwsException } from '@glangeo/pollux/utils';
import { User } from 'src/common/types/user';
import { IUserModel, IUserDAO } from '../../interfaces/user';
import { UserCreatePayload } from '../../types';

@bindMethods
export class UserModel implements IUserModel {
  public constructor(protected readonly dao: IUserDAO) {}

  public async create(payload: UserCreatePayload): Promise<User> {
    const isEmailUnique = await throwsException(
      () => this.dao.getByEmail(payload.email),
      NotFoundException
    );

    if (!isEmailUnique) {
      throw new ValidationException({
        message: 'Email is not unique.',
        publicInfo: {
          message: 'Email is alredy in use.',
        },
      });
    }

    const user = await this.dao.create(payload);

    return user;
  }

  public getById(id: number): Promise<User> {
    return this.dao.getById(id);
  }

  public getAll(): Promise<User[]> {
    return this.dao.getAll();
  }

  public updatePersonalInfo(
    id: number,
    updates: Pick<
      User,
      'phone' | 'firstName' | 'lastName' | 'dateOfBirth' | 'jobTitle'
    >
  ): Promise<boolean> {
    return this.dao.updatePersonalInfo(id, updates);
  }
}

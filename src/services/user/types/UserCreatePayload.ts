import { User } from 'src/common/types/user';

export type UserCreatePayload = Pick<
  User,
  'email' | 'phone' | 'firstName' | 'lastName' | 'dateOfBirth' | 'jobTitle'
>;

import { MongoDB } from '@glangeo/pollux/db/drivers/mongo';
import { User } from 'src/common/types/user';
import { IUserDAO } from '../../interfaces/user';
import { UserCreatePayload } from '../../types';
import { getUserCollectionAdapter } from './getUserCollectionAdapter';

export class UserDAO implements IUserDAO {
  protected readonly adapter: ReturnType<typeof getUserCollectionAdapter>;

  public constructor(db: MongoDB) {
    this.adapter = getUserCollectionAdapter(db);
  }

  public getById(id: number): Promise<User> {
    return this.adapter.getOne({ id });
  }

  public getAll(): Promise<User[]> {
    return this.adapter.getAll();
  }

  public create(payload: UserCreatePayload): Promise<User> {
    return this.adapter.create(payload);
  }

  public getByEmail(email: User['email']): Promise<User> {
    return this.adapter.getOne({ email });
  }

  public updatePersonalInfo(
    id: number,
    updates: Pick<
      User,
      'phone' | 'firstName' | 'lastName' | 'dateOfBirth' | 'jobTitle'
    >
  ): Promise<boolean> {
    return this.adapter.updateOne({ id }, { $set: updates });
  }
}

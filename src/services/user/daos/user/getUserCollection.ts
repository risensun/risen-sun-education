import {
  createCollection,
  generateEntityId,
  MongoDB,
  RecordSchema,
} from '@glangeo/pollux/db/drivers/mongo';
import { WithId } from 'mongodb';
import { User } from 'src/common/types/user';

export type UserRecord = RecordSchema<WithId<User>>;

export function getUserCollection(db: MongoDB) {
  return createCollection({
    name: 'user__users',

    createEntityFromDBRecord(record: UserRecord): WithId<User> {
      return record;
    },

    async getRecordDefaultFields() {
      return {
        id: await generateEntityId(db.getDb(), this.name),
        createdAt: Date.now(),
        isActive: true,
      };
    },
  });
}

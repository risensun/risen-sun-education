import {
  getCollectionAdapter,
  MongoDB,
} from '@glangeo/pollux/db/drivers/mongo';
import { getUserCollection } from './getUserCollection';

export function getUserCollectionAdapter(db: MongoDB) {
  return getCollectionAdapter(db.getDb(), getUserCollection(db));
}

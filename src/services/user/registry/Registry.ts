import { RegistryFactory } from '@glangeo/pollux';
import { ServiceRegistry } from '@glangeo/pollux/distributed/http';
import { CommonRegistry } from 'src/CommonRegistry';
import { AuthService } from 'src/services/auth/AuthService';
import { UserDAO } from '../daos/user';
import { IUserModel } from '../interfaces/user';
import { UserModel } from '../models/user';

export const Registry = new RegistryFactory()
  .addMethod({
    key: 'getMongoDb',
    factory: () => CommonRegistry.getMongoDb(),
  })
  .addMethod({
    key: 'getUserModel',
    factory: (registry): IUserModel =>
      new UserModel(new UserDAO(registry.getMongoDb())),
  })
  .addMethod({
    key: 'getAuthService',
    factory: (): AuthService => ServiceRegistry.getService(AuthService),
  })
  .build();

import { SkillTreeLabel } from 'src/common/types/skill';
import { Registry } from './registry';
import { SkillTreeNode } from './types';

export class SkillService {
  public getSkillById(
    id: number
  ): Promise<SkillTreeNode<SkillTreeLabel.Skill>> {
    return Registry.getSkillTreeModel().getByIdAndLabel(
      id,
      SkillTreeLabel.Skill
    );
  }

  /** Aliases */
  public createSkill = Registry.getSkillTreeModel().createSkill;
  public createFolder = Registry.getSkillTreeModel().createFolder;
  public getBreadcrumbs = Registry.getSkillTreeModel().getBreadcrumbs;
  public getNodeById = Registry.getSkillTreeModel().getById;
  public getNodeChildren = Registry.getSkillTreeModel().getChildren;
  public getSkills = Registry.getSkillTreeModel().getSkills;
  public updateSkill = Registry.getSkillTreeModel().updateSkill;
  public moveToFolderOrRoot = Registry.getSkillTreeModel().moveToFolderOrRoot;
}

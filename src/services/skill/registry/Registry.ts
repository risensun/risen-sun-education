import { RegistryFactory } from '@glangeo/pollux';
import { CommonRegistry } from 'src/CommonRegistry';
import { ISkillTreeModel } from '../interfaces';
import { SkillTreeModel } from '../models';
import { TreeDAO } from '../utils';

export const Registry = new RegistryFactory()
  .addMethod({
    key: 'getMongoDb',
    factory: () => CommonRegistry.getMongoDb(),
  })
  .addMethod({
    key: 'getSkillTreeModel',
    factory: (registry): ISkillTreeModel =>
      new SkillTreeModel(new TreeDAO(registry.getMongoDb(), 'skills')),
  })
  .build();

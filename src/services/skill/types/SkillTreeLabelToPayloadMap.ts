import { Skill, SkillFolder, SkillTreeLabel } from 'src/common/types/skill';

export type SkillTreeLabelToPayloadMap = {
  [SkillTreeLabel.Skill]: Skill;
  [SkillTreeLabel.Folder]: SkillFolder;
};

import { SkillTreeLabel } from 'src/common/types/skill';
import { Tree } from '../utils';
import { SkillTreeLabelToPayloadMap } from './SkillTreeLabelToPayloadMap';

export type SkillTreeNode<L extends SkillTreeLabel = SkillTreeLabel> =
  Tree.Node<L, SkillTreeLabelToPayloadMap>;

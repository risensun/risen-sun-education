import { ValidationException } from '@glangeo/pollux';
import { bindMethods } from '@glangeo/pollux/utils';
import { Skill, SkillFolder, SkillTreeLabel } from 'src/common/types/skill';
import { Optional } from 'utility-types';
import { ISkillTreeModel } from '../interfaces';
import { SkillTreeLabelToPayloadMap, SkillTreeNode } from '../types';
import { ITreeDAO } from '../utils';

@bindMethods
export class SkillTreeModel implements ISkillTreeModel {
  public constructor(
    protected readonly tree: ITreeDAO<
      SkillTreeLabel,
      SkillTreeLabelToPayloadMap
    >
  ) {}

  public async createSkill(
    data: Pick<Skill, 'name' | 'description'> &
      Pick<Optional<Skill>, 'attachments'>,
    folderId: number | null
  ): Promise<SkillTreeNode> {
    const { name, description, attachments = [] } = data;

    const node = await this.tree.create({
      children: [],
      parentId: folderId,
      label: SkillTreeLabel.Skill,
      data: {
        name,
        description,
        attachments,
        createdAt: Date.now(),
      },
    });

    return node;
  }

  public async createFolder(
    data: Pick<SkillFolder, 'name'>,
    folderId: number | null
  ): Promise<SkillTreeNode> {
    const { name } = data;

    const node = await this.tree.create({
      children: [],
      parentId: folderId,
      label: SkillTreeLabel.Folder,
      data: {
        name,
        createdAt: Date.now(),
      },
    });

    return node;
  }

  public async getById(id: number): Promise<SkillTreeNode> {
    return this.tree.getById(id);
  }

  public async getByIdAndLabel<L extends SkillTreeLabel>(
    id: SkillTreeNode['id'],
    label: L
  ): Promise<SkillTreeNode<L>> {
    return this.tree.getByIdAndLabel(id, label);
  }

  public async getBreadcrumbs(id: number): Promise<SkillTreeNode[]> {
    const node = await this.tree.getById(id);
    const pathToRoot = await this.tree.getPathToRoot(node.id);

    return pathToRoot;
  }

  public async getChildren(
    id: SkillTreeNode['id'] | null
  ): Promise<SkillTreeNode<SkillTreeLabel>[]> {
    return this.tree.getByParentId(id);
  }

  public async getSkills(): Promise<SkillTreeNode<SkillTreeLabel.Skill>[]> {
    const nodes = await this.tree.getByLabel(SkillTreeLabel.Skill);

    return nodes;
  }

  public async updateSkill(
    id: number,
    updates: Pick<Skill, 'name' | 'description' | 'attachments'>
  ): Promise<boolean> {
    return this.tree.updateDataByIdAndLabel(id, SkillTreeLabel.Skill, updates);
  }

  public async updateFolder(
    id: number,
    updates: Pick<SkillFolder, 'name'>
  ): Promise<boolean> {
    return this.tree.updateDataByIdAndLabel(id, SkillTreeLabel.Folder, updates);
  }

  public async moveToFolderOrRoot(
    id: SkillTreeNode['id'],
    parentId: SkillTreeNode['id'] | null
  ): Promise<boolean> {
    if (id === parentId) {
      throw new ValidationException({
        message: 'Could not move node inside itself.',
        publicInfo: {
          message: 'Could not move node inside itself.',
        },
      });
    }

    if (!parentId) {
      return this.tree.setParent(id, null);
    }

    const parent = await this.getById(parentId);

    if (parent.label !== SkillTreeLabel.Folder) {
      throw new ValidationException({
        message: `Could not add children to ${parent.label}.`,
        meta: {
          errors: ['You only could add nested items to folders.'],
        },
        publicInfo: {
          message: `Only folders can contain nested items.`,
        },
      });
    }

    return this.tree.setParent(id, parentId);
  }
}

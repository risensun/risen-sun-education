import { Optional } from 'utility-types';
import { Skill, SkillFolder, SkillTreeLabel } from 'src/common/types/skill';
import { SkillTreeNode } from '../types';

export interface ISkillTreeModel {
  createSkill(
    data: Pick<Skill, 'name' | 'description'> &
      Optional<Pick<Skill, 'attachments'>>,
    folderId: number | null
  ): Promise<SkillTreeNode>;

  createFolder(
    data: Pick<SkillFolder, 'name'>,
    folderId: number | null
  ): Promise<SkillTreeNode>;

  getById(id: SkillTreeNode['id']): Promise<SkillTreeNode>;

  getByIdAndLabel<L extends SkillTreeLabel>(
    id: SkillTreeNode['id'],
    label: L
  ): Promise<SkillTreeNode<L>>;

  getBreadcrumbs(id: SkillTreeNode['id']): Promise<SkillTreeNode[]>;

  getChildren(id: SkillTreeNode['id'] | null): Promise<SkillTreeNode[]>;

  getSkills(): Promise<SkillTreeNode<SkillTreeLabel.Skill>[]>;

  updateSkill(
    id: SkillTreeNode['id'],
    updates: Pick<Skill, 'name' | 'description' | 'attachments'>
  ): Promise<boolean>;

  updateFolder(
    id: SkillTreeNode['id'],
    updates: Pick<SkillFolder, 'name'>
  ): Promise<boolean>;

  moveToFolderOrRoot(
    id: SkillTreeNode['id'],
    parentId: SkillTreeNode['id'] | null
  ): Promise<boolean>;
}

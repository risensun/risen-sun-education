import { Label as _Label } from './Label';
import { LabelToDataMap as _LabelToDataMap } from './LabelToDataMap';
import { Node as _Node } from './Node';

export namespace Tree {
  export type Label = _Label;
  export type LabelToDataMap<L extends Label> = _LabelToDataMap<L>;
  export type Node<L extends Label, M extends LabelToDataMap<L>> = _Node<L, M>;
}

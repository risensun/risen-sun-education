import { Label } from './Label';

export type LabelToDataMap<L extends Label> = {
  [K in L]: any;
};

import { EntitySchema } from '@glangeo/pollux/db/drivers/mongo';
import { Label } from './Label';
import { LabelToDataMap } from './LabelToDataMap';

export type Node<
  L extends Label,
  M extends LabelToDataMap<L>
> = EntitySchema & {
  readonly id: number;
  readonly parentId: number | null;
  readonly children: number[];
  readonly label: L;
  readonly data: M[L];
};

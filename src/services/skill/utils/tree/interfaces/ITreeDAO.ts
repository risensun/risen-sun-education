import { Optional } from 'utility-types';
import { Tree } from '../types';

export interface ITreeDAO<
  L extends Tree.Label,
  M extends Tree.LabelToDataMap<L>
> {
  create(
    data: Pick<Tree.Node<L, M>, 'children' | 'data' | 'label' | 'parentId'>
  ): Promise<Tree.Node<L, M>>;

  getById(id: Tree.Node<L, M>['id']): Promise<Tree.Node<L, M>>;

  getByIdAndLabel<FL extends L>(
    id: Tree.Node<L, M>['id'],
    label: FL
  ): Promise<Tree.Node<FL, M>>;

  getPathToRoot(id: Tree.Node<L, M>['id']): Promise<Tree.Node<L, M>[]>;

  getByParentId(parentId: number | null): Promise<Tree.Node<L, M>[]>;

  getByLabel<FL extends L>(label: FL): Promise<Tree.Node<FL, M>[]>;

  setParent(
    id: Tree.Node<L, M>['id'],
    parentId: Tree.Node<L, M>['id'] | null
  ): Promise<boolean>;

  updateDataByIdAndLabel(
    id: Tree.Node<L, M>['id'],
    label: L,
    data: Optional<M[L]>
  ): Promise<boolean>;
}

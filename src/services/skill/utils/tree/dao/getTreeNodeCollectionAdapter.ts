import {
  createCollection,
  generateEntityId,
  getCollectionAdapter,
  MongoDB,
  RecordSchema,
} from '@glangeo/pollux/db/drivers/mongo';
import { Tree } from '../types';

type TreeNodeRecord<
  L extends Tree.Label,
  M extends Tree.LabelToDataMap<L>
> = RecordSchema<Tree.Node<L, M>>;

function getTreeNodeCollection<
  L extends Tree.Label,
  M extends Tree.LabelToDataMap<L>
>(db: MongoDB, prefix: string) {
  return createCollection({
    name: `${prefix}__tree`,

    createEntityFromDBRecord(record: TreeNodeRecord<L, M>): Tree.Node<L, M> {
      return record;
    },

    async getRecordDefaultFields() {
      return {
        id: await generateEntityId(db.getDb(), this.name),
      };
    },
  });
}

export function getTreeNodeCollectionAdapter<
  L extends Tree.Label,
  M extends Tree.LabelToDataMap<L>
>(db: MongoDB, prefix: string) {
  return getCollectionAdapter(
    db.getDb(),
    getTreeNodeCollection<L, M>(db, prefix)
  );
}

export class __ExtractTreeNodeType<
  L extends Tree.Label,
  M extends Tree.LabelToDataMap<L>
> {
  public extractAdapterType() {
    return getTreeNodeCollectionAdapter<L, M>(null as any, '');
  }
}

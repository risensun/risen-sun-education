import { MongoDB } from '@glangeo/pollux/db/drivers/mongo';
import { Optional } from 'utility-types';
import { ITreeDAO } from '../interfaces';
import { Tree } from '../types';
import {
  getTreeNodeCollectionAdapter,
  __ExtractTreeNodeType,
} from './getTreeNodeCollectionAdapter';

export class TreeDAO<L extends Tree.Label, M extends Tree.LabelToDataMap<L>>
  implements ITreeDAO<L, M>
{
  private readonly adapter: ReturnType<
    __ExtractTreeNodeType<L, M>['extractAdapterType']
  >;

  public constructor(db: MongoDB, prefix: string) {
    this.adapter = getTreeNodeCollectionAdapter(db, prefix);
  }

  public async create(
    data: Pick<Tree.Node<L, M>, 'children' | 'data' | 'label' | 'parentId'>
  ): Promise<Tree.Node<L, M>> {
    const node = await this.adapter.create(data);

    return node;
  }

  public async getById(id: Tree.Node<L, M>['id']): Promise<Tree.Node<L, M>> {
    return this.adapter.getOne({ id });
  }

  public async getByIdAndLabel<FL extends L>(
    id: Tree.Node<L, M>['id'],
    label: FL
  ): Promise<Tree.Node<FL, M>> {
    const node = await this.adapter.getOne({ id, label });

    return node as Tree.Node<FL, M>;
  }

  public async getByParentId(
    parentId: number | null
  ): Promise<Tree.Node<L, M>[]> {
    return this.adapter.getMany({ parentId });
  }

  public async getPathToRoot(
    id: Tree.Node<L, M>['id']
  ): Promise<Tree.Node<L, M>[]> {
    const node = await this.getById(id);
    const path: Tree.Node<L, M>[] = [node];

    if (!node.parentId) {
      return path;
    }

    const nodeIdsInPath: number[] = [node.parentId];

    while (nodeIdsInPath.length !== 0) {
      const nextNodeId = nodeIdsInPath.shift();

      if (!nextNodeId) {
        break;
      }

      const node = await this.getById(nextNodeId);

      path.push(node);

      if (node.parentId) {
        nodeIdsInPath.push(node.parentId);
      }
    }

    return path;
  }

  public async getByLabel<FL extends L>(
    label: FL
  ): Promise<Tree.Node<FL, M>[]> {
    const nodes = await this.adapter.getMany({ label });

    return nodes as Tree.Node<FL, M>[];
  }

  public async updateDataByIdAndLabel(
    id: number,
    label: L,
    data: Optional<M[L], keyof M[L]>
  ): Promise<boolean> {
    const node = await this.getById(id);

    if (node.label !== label) {
      return false;
    }

    return this.adapter.updateOne(
      { id },
      { $set: { data: { ...node.data, ...data } } }
    );
  }

  public async setParent(
    id: Tree.Node<L, M>['id'],
    parentId: Tree.Node<L, M>['id'] | null
  ): Promise<boolean> {
    const node = await this.getById(id);

    if (node.parentId) {
      const isSucceeded = await this.removeChild(node.parentId, id);

      if (!isSucceeded) {
        return false;
      }
    }

    const isSucceeded = await this.adapter.updateOne(
      { id },
      { $set: { parentId } }
    );

    if (!isSucceeded) {
      return false;
    }

    if (parentId) {
      return this.addChild(parentId, id);
    }

    return true;
  }

  private async addChild(
    id: Tree.Node<L, M>['id'],
    childId: Tree.Node<L, M>['id']
  ): Promise<boolean> {
    const node = await this.getById(id);

    if (node.children.includes(childId)) {
      return true;
    }

    return this.adapter.updateOne(
      { id },
      { $push: { children: childId as any } }
    );
  }

  private async removeChild(
    id: Tree.Node<L, M>['id'],
    childId: Tree.Node<L, M>['id']
  ): Promise<boolean> {
    const node = await this.getById(id);

    if (!node.children.includes(childId)) {
      return true;
    }

    return this.adapter.updateOne(
      { id },
      { $pull: { children: childId as any } }
    );
  }
}

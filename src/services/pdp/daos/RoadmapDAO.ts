import { MongoDB } from '@glangeo/pollux/db/drivers/mongo';
import { Roadmap, Stage } from 'src/common/types/pdp';
import { RoadmapStatus } from 'src/common/types/pdp/RoadmapStatus';
import { IRoadmapDAO } from '../interfaces';
import { getRoadmapCollectionAdapter } from './getRoadmapCollectionAdapter';

export class RoadmapDAO implements IRoadmapDAO {
  protected readonly adapter: ReturnType<typeof getRoadmapCollectionAdapter>;

  public constructor(db: MongoDB) {
    this.adapter = getRoadmapCollectionAdapter(db);
  }

  public async create(
    roadmap: Pick<
      Roadmap,
      'assigneeId' | 'reviewerId' | 'stages' | 'from' | 'to'
    >
  ): Promise<Roadmap> {
    return this.adapter.create({ ...roadmap });
  }

  public getById(id: number): Promise<Roadmap> {
    return this.adapter.getOne({ id });
  }

  public getByAssigneeId(id: number): Promise<Roadmap[]> {
    return this.adapter.getMany({ assigneeId: id });
  }

  public getByReviewerId(id: number): Promise<Roadmap[]> {
    return this.adapter.getMany({ reviewerId: id });
  }

  public getAll(): Promise<Roadmap[]> {
    return this.adapter.getAll();
  }

  public updateStatus(id: number, status: RoadmapStatus): Promise<boolean> {
    return this.adapter.updateOne({ id }, { $set: { status } });
  }

  public updateFromTo(
    id: number,
    updates: Pick<Roadmap, 'from' | 'to'>
  ): Promise<boolean> {
    return this.adapter.updateOne({ id }, { $set: { ...updates } });
  }

  public renameStage(
    id: number,
    stageKey: string,
    updates: Pick<Stage, 'name'>
  ): Promise<boolean> {
    return this.adapter.updateOne(
      { id, 'stages.key': stageKey },
      { $set: { 'stages.$': { ...updates } } }
    );
  }

  public addSkillToStage(
    id: number,
    stageKey: string,
    skillId: number
  ): Promise<boolean> {
    return this.adapter.updateOne(
      { id, 'stages.key': stageKey },
      {
        $push: {
          'stages.$.skills': { id: skillId, isPassed: false },
        },
      }
    );
  }

  public removeSkillsFromStage(
    id: number,
    stageKey: string,
    skillIds: number[]
  ): Promise<boolean> {
    return this.adapter.updateOne(
      { id, 'stages.key': stageKey },
      {
        $pull: {
          'stages.$.skills': { id: { $in: skillIds } },
        },
      }
    );
  }

  public setSkillsIsPassed(
    id: number,
    stageKey: string,
    skillIds: number[],
    value: boolean
  ): Promise<boolean> {
    return this.adapter.updateOne(
      { id, 'stages.key': stageKey, 'stages.skills.id': { $in: skillIds } },
      { $set: { 'stages.$.skills.$': { isPassed: value } } }
    );
  }

  public changeReviewer(id: number, reviewerId: number): Promise<boolean> {
    return this.adapter.updateOne({ id }, { $set: { reviewerId } });
  }
}

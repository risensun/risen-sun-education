import {
  createCollection,
  generateEntityId,
  getCollectionAdapter,
  MongoDB,
} from '@glangeo/pollux/db/drivers/mongo';
import { WithId } from 'mongodb';
import { Roadmap } from 'src/common/types/pdp';
import { RoadmapStatus } from 'src/common/types/pdp/RoadmapStatus';

type RoadmapRecord = WithId<Roadmap>;

export function getRoadmapCollectionAdapter(db: MongoDB) {
  return getCollectionAdapter(
    db.getDb(),
    createCollection({
      name: 'pdp__roadmaps',

      createEntityFromDBRecord(record: RoadmapRecord): WithId<Roadmap> {
        return record;
      },

      async getRecordDefaultFields() {
        return {
          id: await generateEntityId(db.getDb(), this.name),
          status: RoadmapStatus.NotStarted,
          createdAt: Date.now(),
          startedAt: null,
        };
      },
    })
  );
}

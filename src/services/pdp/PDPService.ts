import { Registry } from './registry';

export class PDPService {
  /** Aliases */
  public create = Registry.getRoadmapModel().create;
  public getById = Registry.getRoadmapModel().getById;
  public getByAssigneeId = Registry.getRoadmapModel().getByAssigneeId;
  public getByReviewerId = Registry.getRoadmapModel().getByReviewerId;
  public getAllRoadmaps = Registry.getRoadmapModel().getAll;
  public markAsFinished = Registry.getRoadmapModel().markAsFinished;
  public updateFromTo = Registry.getRoadmapModel().updateFromTo;
  public renameStage = Registry.getRoadmapModel().renameStage;
  public addSkillToStage = Registry.getRoadmapModel().addSkillToStage;
  public removeSkillsFromStage =
    Registry.getRoadmapModel().removeSkillsFromStage;
  public setSkillsIsPassed = Registry.getRoadmapModel().setSkillsIsPassed;
  public changeReviewer = Registry.getRoadmapModel().changeReviewer;
}

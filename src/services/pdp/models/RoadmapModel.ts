import { nanoid } from 'nanoid';
import { Roadmap, Stage } from 'src/common/types/pdp';
import { RoadmapStatus } from 'src/common/types/pdp/RoadmapStatus';
import { IRoadmapDAO, IRoadmapModel } from '../interfaces';

export class RoadmapModel implements IRoadmapModel {
  public constructor(protected readonly dao: IRoadmapDAO) {}

  public async create(
    roadmap: Pick<Roadmap, 'assigneeId' | 'reviewerId' | 'from' | 'to'>,
    stages: Pick<Stage, 'name' | 'skills'>[]
  ): Promise<Roadmap> {
    return this.dao.create({
      ...roadmap,
      stages: stages.map(({ name, skills }) => ({
        key: nanoid(),
        name,
        skills,
        isPassed: false,
        createdAt: Date.now(),
      })),
    });
  }

  public async getById(id: number): Promise<Roadmap> {
    return this.dao.getById(id);
  }

  public async getByAssigneeId(id: number): Promise<Roadmap[]> {
    return this.dao.getByAssigneeId(id);
  }

  public async getByReviewerId(id: number): Promise<Roadmap[]> {
    return this.dao.getByReviewerId(id);
  }

  public async getAll(): Promise<Roadmap[]> {
    return this.dao.getAll();
  }

  public async markAsFinished(id: number): Promise<boolean> {
    return this.dao.updateStatus(id, RoadmapStatus.Finished);
  }

  public async updateFromTo(
    id: number,
    updates: Pick<Roadmap, 'from' | 'to'>
  ): Promise<boolean> {
    return this.dao.updateFromTo(id, updates);
  }

  public async renameStage(
    id: number,
    stageKey: string,
    updates: Pick<Stage, 'name'>
  ): Promise<boolean> {
    return this.dao.renameStage(id, stageKey, updates);
  }

  public async addSkillToStage(
    id: number,
    stageKey: string,
    skillId: number
  ): Promise<boolean> {
    return this.dao.addSkillToStage(id, stageKey, skillId);
  }

  public async removeSkillsFromStage(
    id: number,
    stageKey: string,
    skillIds: number[]
  ): Promise<boolean> {
    return this.dao.removeSkillsFromStage(id, stageKey, skillIds);
  }

  public async setSkillsIsPassed(
    id: number,
    stageKey: string,
    skillIds: number[],
    value: boolean
  ): Promise<boolean> {
    return this.dao.setSkillsIsPassed(id, stageKey, skillIds, value);
  }

  public async changeReviewer(
    id: number,
    reviewerId: number
  ): Promise<boolean> {
    // TODO: add extra logic
    return this.dao.changeReviewer(id, reviewerId);
  }
}

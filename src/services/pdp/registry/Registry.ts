import { RegistryFactory } from '@glangeo/pollux';
import { CommonRegistry } from 'src/CommonRegistry';
import { RoadmapDAO } from '../daos';
import { IRoadmapModel } from '../interfaces';
import { RoadmapModel } from '../models';

export const Registry = new RegistryFactory()
  .addMethod({
    key: 'getMongoDb',
    factory: () => CommonRegistry.getMongoDb(),
  })
  .addMethod({
    key: 'getRoadmapModel',
    factory: (registry): IRoadmapModel =>
      new RoadmapModel(new RoadmapDAO(registry.getMongoDb())),
  })
  .build();

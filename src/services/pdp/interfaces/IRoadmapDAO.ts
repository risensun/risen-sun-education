import { IEntityDAO } from 'src/common';
import { Roadmap, Stage } from 'src/common/types/pdp';
import { RoadmapStatus } from 'src/common/types/pdp/RoadmapStatus';

export interface IRoadmapDAO extends IEntityDAO<Roadmap> {
  create(
    roadmap: Pick<
      Roadmap,
      'assigneeId' | 'reviewerId' | 'from' | 'to' | 'stages'
    >
  ): Promise<Roadmap>;

  getByAssigneeId(id: number): Promise<Roadmap[]>;

  getByReviewerId(id: number): Promise<Roadmap[]>;

  getAll(): Promise<Roadmap[]>;

  updateStatus(id: Roadmap['id'], status: RoadmapStatus): Promise<boolean>;

  updateFromTo(
    id: Roadmap['id'],
    updates: Pick<Roadmap, 'from' | 'to'>
  ): Promise<boolean>;

  renameStage(
    id: Roadmap['id'],
    stageKey: Stage['key'],
    updates: Pick<Stage, 'name'>
  ): Promise<boolean>;

  addSkillToStage(
    id: Roadmap['id'],
    stageKey: Stage['key'],
    skillId: number
  ): Promise<boolean>;

  removeSkillsFromStage(
    id: Roadmap['id'],
    stageKey: Stage['key'],
    skillIds: number[]
  ): Promise<boolean>;

  setSkillsIsPassed(
    id: Roadmap['id'],
    stageKey: Stage['key'],
    skillIds: number[],
    value: Stage['skills'][0]['isPassed']
  ): Promise<boolean>;

  changeReviewer(id: Roadmap['id'], reviewerId: number): Promise<boolean>;
}

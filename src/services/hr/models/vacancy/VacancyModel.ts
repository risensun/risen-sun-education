import { bindMethods } from '@glangeo/pollux/utils';
import { Vacancy } from 'src/common/types/hr';
import { IVacancyDAO, IVacancyModel } from '../../interfaces/vacancy';

@bindMethods
export class VacancyModel implements IVacancyModel {
  public constructor(protected readonly dao: IVacancyDAO) {}

  public create(
    data: Omit<
      Vacancy,
      'id' | 'createdAt' | 'isPublished' | 'candidateIds' | 'isDeleted'
    >
  ): Promise<Vacancy> {
    return this.dao.create(data);
  }

  public getById(id: number): Promise<Vacancy> {
    return this.dao.getById(id);
  }

  public getPublished(): Promise<Vacancy[]> {
    return this.dao.getPublished();
  }

  public getPrivate(): Promise<Vacancy[]> {
    return this.dao.getPrivate();
  }

  public updateInfo(
    id: number,
    update: Omit<
      Vacancy,
      'id' | 'createdAt' | 'isPublished' | 'candidateIds' | 'isDeleted'
    >
  ): Promise<boolean> {
    return this.dao.updateInfo(id, update);
  }

  public updatePublishing(id: number, isPublished: boolean): Promise<boolean> {
    return this.dao.updatePublishing(id, isPublished);
  }

  public addCandidate(
    id: Vacancy['id'],
    candidateId: number
  ): Promise<boolean> {
    return this.dao.addCandidate(id, candidateId);
  }
}

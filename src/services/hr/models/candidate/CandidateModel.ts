import { NotFoundException, ValidationException } from '@glangeo/pollux';
import { bindMethods, throwsException } from '@glangeo/pollux/utils';
import { Candidate, CandidateStatus } from 'src/common/types/hr';
import { ICandidateDAO, ICandidateModel } from '../../interfaces/candidate';

@bindMethods
export class CandidateModel implements ICandidateModel {
  public constructor(protected readonly dao: ICandidateDAO) {}

  public async create(
    data: Omit<Candidate, 'id' | 'createdAt' | 'status' | 'isDeleted'>
  ): Promise<Candidate> {
    const isEmailUnique = await throwsException(
      () => this.dao.getByEmail(data.email),
      NotFoundException
    );

    if (!isEmailUnique) {
      throw new ValidationException({
        message: 'Email is not unique.',
        meta: {
          errors: ['This candidate already applied for any of the vacancies.'],
        },
        publicInfo: {
          message: 'Candidate already applied for any of the vacancies.',
        },
      });
    }

    return this.dao.create(data);
  }

  public getById(id: number): Promise<Candidate> {
    return this.dao.getById(id);
  }

  public getByIds(ids: number[]): Promise<Candidate[]> {
    return this.dao.getByIds(ids);
  }

  public getCurrent(): Promise<Candidate[]> {
    return this.dao.getCurrent();
  }

  public getArchived(): Promise<Candidate[]> {
    return this.dao.getArchived();
  }

  public updateInfo(
    id: number,
    update: Omit<Candidate, 'id' | 'createdAt' | 'status' | 'isDeleted'>
  ): Promise<boolean> {
    return this.dao.updateInfo(id, update);
  }

  public updateStatus(id: number, status: CandidateStatus): Promise<boolean> {
    return this.dao.updateStatus(id, status);
  }
}

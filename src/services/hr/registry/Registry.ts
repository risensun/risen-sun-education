import { RegistryFactory } from '@glangeo/pollux';
import { ServiceRegistry } from '@glangeo/pollux/distributed/http';
import { CommonRegistry } from 'src/CommonRegistry';
import { AuthService } from 'src/services/auth/AuthService';
import { UserService } from 'src/services/user/UserService';
import { CandidateDAO } from '../daos/candidate';
import { VacancyDAO } from '../daos/vacancy';
import { ICandidateModel } from '../interfaces/candidate';
import { CandidateModel } from '../models/candidate';
import { VacancyModel } from '../models/vacancy';

export const Registry = new RegistryFactory()
  .addMethod({
    key: 'getMongoDb',
    factory: () => CommonRegistry.getMongoDb(),
  })
  .addMethod({
    key: 'getVacancyModel',
    factory: (registry) =>
      new VacancyModel(new VacancyDAO(registry.getMongoDb())),
  })
  .addMethod({
    key: 'getCandidateModel',
    factory: (registry): ICandidateModel =>
      new CandidateModel(new CandidateDAO(registry.getMongoDb())),
  })
  .addMethod({
    key: 'getAuthService',
    factory: () => ServiceRegistry.getService(AuthService),
  })
  .addMethod({
    key: 'getUserService',
    factory: () => ServiceRegistry.getService(UserService),
  })
  .build();

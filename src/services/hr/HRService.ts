import { Candidate, Vacancy } from 'src/common/types/hr';
import { User } from 'src/common/types/user';
import { Registry } from './registry';

export class HRService {
  public async getCandidates(isCurrent: boolean): Promise<Candidate[]> {
    return isCurrent
      ? Registry.getCandidateModel().getCurrent()
      : Registry.getCandidateModel().getArchived();
  }

  public async getVacancies(isPublished: boolean): Promise<Vacancy[]> {
    return isPublished
      ? Registry.getVacancyModel().getPublished()
      : Registry.getVacancyModel().getPrivate();
  }

  public async getVacancyCandidates(vacancyId: number): Promise<Candidate[]> {
    const vacancy = await Registry.getVacancyModel().getById(vacancyId);
    const candidates = await Registry.getCandidateModel().getByIds(
      vacancy.candidateIds
    );

    return candidates;
  }

  public async hireCandidate(
    id: Candidate['id'],
    jobTitle: string
  ): Promise<User> {
    const candidate = await Registry.getCandidateModel().getById(id);
    const user = await Registry.getUserService().createUser({
      jobTitle,
      firstName: candidate.firstName,
      lastName: candidate.lastName,
      email: candidate.email,
      phone: candidate.phone,
      dateOfBirth: candidate.dateOfBirth,
    });

    await Registry.getAuthService().register(user.email, '12345678');

    return user;
  }

  /** Aliases */
  public createVacancy = Registry.getVacancyModel().create;
  public getVacancyById = Registry.getVacancyModel().getById;
  public addCandidateToVacancy = Registry.getVacancyModel().addCandidate;
  public getCandidateById = Registry.getCandidateModel().getById;
  public updateCandidate = Registry.getCandidateModel().updateInfo;
  public updateCandidateStatus = Registry.getCandidateModel().updateStatus;
}

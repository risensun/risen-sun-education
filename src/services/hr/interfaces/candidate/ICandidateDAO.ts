import { IEntityDAO } from 'src/common';
import { Candidate, CandidateStatus } from 'src/common/types/hr';

export interface ICandidateDAO extends IEntityDAO<Candidate> {
  create(
    data: Omit<Candidate, 'id' | 'createdAt' | 'status' | 'isDeleted'>
  ): Promise<Candidate>;

  getByEmail(email: Candidate['email']): Promise<Candidate>;

  getByIds(ids: Candidate['id'][]): Promise<Candidate[]>;

  getCurrent(): Promise<Candidate[]>;

  getArchived(): Promise<Candidate[]>;

  updateInfo(
    id: Candidate['id'],
    update: Omit<Candidate, 'id' | 'createdAt' | 'isDeleted' | 'status'>
  ): Promise<boolean>;

  updateStatus(id: Candidate['id'], status: CandidateStatus): Promise<boolean>;
}

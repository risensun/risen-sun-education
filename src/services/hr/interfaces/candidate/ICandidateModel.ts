import { Candidate, CandidateStatus } from 'src/common/types/hr';

export interface ICandidateModel {
  create(
    data: Omit<Candidate, 'id' | 'createdAt' | 'status' | 'isDeleted'>
  ): Promise<Candidate>;

  getById(id: Candidate['id']): Promise<Candidate>;

  getByIds(ids: Candidate['id'][]): Promise<Candidate[]>;

  getCurrent(): Promise<Candidate[]>;

  getArchived(): Promise<Candidate[]>;

  updateInfo(
    id: Candidate['id'],
    update: Omit<Candidate, 'id' | 'createdAt' | 'isDeleted' | 'status'>
  ): Promise<boolean>;

  updateStatus(id: Candidate['id'], status: CandidateStatus): Promise<boolean>;
}

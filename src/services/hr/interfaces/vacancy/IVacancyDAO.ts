import { IEntityDAO } from 'src/common';
import { Vacancy } from 'src/common/types/hr';

export interface IVacancyDAO extends IEntityDAO<Vacancy> {
  create(
    data: Omit<
      Vacancy,
      'id' | 'createdAt' | 'isPublished' | 'candidateIds' | 'isDeleted'
    >
  ): Promise<Vacancy>;

  getPublished(): Promise<Vacancy[]>;

  getPrivate(): Promise<Vacancy[]>;

  updateInfo(
    id: Vacancy['id'],
    update: Omit<
      Vacancy,
      'id' | 'createdAt' | 'isPublished' | 'candidateIds' | 'isDeleted'
    >
  ): Promise<boolean>;

  updatePublishing(id: Vacancy['id'], isPublished: boolean): Promise<boolean>;

  addCandidate(id: Vacancy['id'], candidateId: number): Promise<boolean>;
}

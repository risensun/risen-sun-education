import { Vacancy } from 'src/common/types/hr';

export interface IVacancyModel {
  create(
    data: Omit<
      Vacancy,
      'id' | 'createdAt' | 'isPublished' | 'candidateIds' | 'isDeleted'
    >
  ): Promise<Vacancy>;

  getById(id: Vacancy['id']): Promise<Vacancy>;

  getPublished(): Promise<Vacancy[]>;

  getPrivate(): Promise<Vacancy[]>;

  updateInfo(
    id: Vacancy['id'],
    update: Omit<
      Vacancy,
      'id' | 'createdAt' | 'isPublished' | 'candidateIds' | 'isDeleted'
    >
  ): Promise<boolean>;

  updatePublishing(id: Vacancy['id'], isPublished: boolean): Promise<boolean>;

  addCandidate(id: Vacancy['id'], candidateId: number): Promise<boolean>;
}

import {
  getCollectionAdapter,
  MongoDB,
} from '@glangeo/pollux/db/drivers/mongo';
import { getCandidateCollection } from './getCandidateCollection';

export function getCandidateCollectionAdapter(db: MongoDB) {
  return getCollectionAdapter(db.getDb(), getCandidateCollection(db));
}

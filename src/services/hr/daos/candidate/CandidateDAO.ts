import { MongoDB } from '@glangeo/pollux/db/drivers/mongo';
import { Candidate, CandidateStatus } from 'src/common/types/hr';
import { ICandidateDAO } from '../../interfaces/candidate';
import { getCandidateCollectionAdapter } from './getCandidateCollectionAdapter';

export class CandidateDAO implements ICandidateDAO {
  protected readonly adapter: ReturnType<typeof getCandidateCollectionAdapter>;

  public constructor(db: MongoDB) {
    this.adapter = getCandidateCollectionAdapter(db);
  }

  public create(
    data: Omit<Candidate, 'id' | 'status' | 'createdAt' | 'isDeleted'>
  ): Promise<Candidate> {
    return this.adapter.create(data);
  }

  public getById(id: number): Promise<Candidate> {
    return this.adapter.getOne({ id });
  }

  public getByIds(ids: number[]): Promise<Candidate[]> {
    return this.adapter.getMany({ id: { $in: ids } });
  }

  public getByEmail(email: string): Promise<Candidate> {
    return this.adapter.getOne({ email, isDeleted: false });
  }

  public getCurrent(): Promise<Candidate[]> {
    return this.adapter.getMany({
      status: {
        $in: [CandidateStatus.HRInterview, CandidateStatus.ProfessionalReview],
      },
      isDeleted: false,
    });
  }

  public getArchived(): Promise<Candidate[]> {
    return this.adapter.getMany({
      status: {
        $in: [CandidateStatus.Declined, CandidateStatus.Accepted],
      },
      isDeleted: false,
    });
  }

  public updateInfo(
    id: number,
    update: Omit<Candidate, 'id' | 'status' | 'createdAt' | 'isDeleted'>
  ): Promise<boolean> {
    return this.adapter.updateOne({ id }, { $set: update });
  }

  public updateStatus(id: number, status: CandidateStatus): Promise<boolean> {
    return this.adapter.updateOne({ id }, { $set: { status } });
  }
}

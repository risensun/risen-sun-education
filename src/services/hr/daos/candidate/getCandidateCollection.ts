import {
  createCollection,
  generateEntityId,
  MongoDB,
  RecordSchema,
} from '@glangeo/pollux/db/drivers/mongo';
import { WithId } from 'mongodb';
import { Candidate, CandidateStatus } from 'src/common/types/hr';

export type CandidateRecord = RecordSchema<WithId<Candidate>>;

export function getCandidateCollection(db: MongoDB) {
  return createCollection({
    name: 'hr__candidates',

    createEntityFromDBRecord(record: CandidateRecord): WithId<Candidate> {
      return record;
    },

    async getRecordDefaultFields() {
      return {
        id: await generateEntityId(db.getDb(), this.name),
        status: CandidateStatus.HRInterview,
        createdAt: Date.now(),
        isDeleted: false,
      };
    },
  });
}

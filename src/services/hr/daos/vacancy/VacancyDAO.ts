import { MongoDB } from '@glangeo/pollux/db/drivers/mongo';
import { Vacancy } from 'src/common/types/hr';
import { IVacancyDAO } from '../../interfaces/vacancy';
import { getVacancyCollectionAdapter } from './getVacancyCollectionAdapter';

export class VacancyDAO implements IVacancyDAO {
  protected readonly adapter: ReturnType<typeof getVacancyCollectionAdapter>;

  public constructor(db: MongoDB) {
    this.adapter = getVacancyCollectionAdapter(db);
  }

  public create(
    data: Omit<
      Vacancy,
      'id' | 'createdAt' | 'isPublished' | 'candidateIds' | 'isDeleted'
    >
  ): Promise<Vacancy> {
    return this.adapter.create(data);
  }

  public getById(id: number): Promise<Vacancy> {
    return this.adapter.getOne({ id });
  }

  public getPublished(): Promise<Vacancy[]> {
    return this.adapter.getMany({ isPublished: true, isDeleted: false });
  }

  public getPrivate(): Promise<Vacancy[]> {
    return this.adapter.getMany({ isPublished: false, isDeleted: false });
  }

  public updateInfo(
    id: number,
    update: Omit<
      Vacancy,
      'id' | 'candidateIds' | 'createdAt' | 'isPublished' | 'isDeleted'
    >
  ): Promise<boolean> {
    return this.adapter.updateOne({ id }, { $set: update });
  }

  public updatePublishing(id: number, isPublished: boolean): Promise<boolean> {
    return this.adapter.updateOne({ id }, { $set: { isPublished } });
  }

  public addCandidate(id: number, candidateId: number): Promise<boolean> {
    return this.adapter.updateOne(
      { id },
      {
        $push: {
          candidateIds: candidateId,
        },
      }
    );
  }
}

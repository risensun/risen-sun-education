import {
  createCollection,
  generateEntityId,
  MongoDB,
  RecordSchema,
} from '@glangeo/pollux/db/drivers/mongo';
import { WithId } from 'mongodb';
import { Vacancy } from 'src/common/types/hr';

export type VacancyRecord = RecordSchema<WithId<Vacancy>>;

export function getVacancyCollection(db: MongoDB) {
  return createCollection({
    name: 'hr__vacancies',

    createEntityFromDBRecord(record: VacancyRecord): WithId<Vacancy> {
      return record;
    },

    async getRecordDefaultFields() {
      return {
        id: await generateEntityId(db.getDb(), this.name),
        candidateIds: [] as number[],
        createdAt: Date.now(),
        isPublished: false,
        isDeleted: false,
      };
    },
  });
}

import {
  getCollectionAdapter,
  MongoDB,
} from '@glangeo/pollux/db/drivers/mongo';
import { getVacancyCollection } from './getVacancyCollection';

export function getVacancyCollectionAdapter(db: MongoDB) {
  return getCollectionAdapter(db.getDb(), getVacancyCollection(db));
}

import { getCollectionAdapter } from '@glangeo/pollux/db/drivers/mongo';
import {
  ClientDAO,
  ClientModel,
  CredentialsDAO,
  CredentialsModel,
  getAuthenticaionFlow,
  getAuthorizationFlow,
  getClientCollection,
  getCredentialsCollection,
} from '@glangeo/pollux/plugins/auth/prebuild';
import {
  Client,
  Credentials,
  CSRF,
  JWT,
  PBKDF2,
} from '@glangeo/pollux/plugins/auth';
import { Config } from 'src/Config';
import { ClientMeta } from 'src/common/types/auth';
import { Registry } from './registry';

export class AuthService {
  protected readonly credentialsModel: CredentialsModel;
  protected readonly clientModel: ClientModel<string, null, ClientMeta>;

  public constructor() {
    const mongoDb = Registry.getMongoDb();

    this.credentialsModel = new CredentialsModel(
      new CredentialsDAO(
        getCollectionAdapter(mongoDb.getDb(), getCredentialsCollection(mongoDb))
      ),
      new PBKDF2()
    );
    this.clientModel = new ClientModel(
      new ClientDAO(
        getCollectionAdapter(
          mongoDb.getDb(),
          getClientCollection<string, null, ClientMeta>(mongoDb)
        )
      )
    );
  }

  public async register(email: string, password: string): Promise<Credentials> {
    return this.credentialsModel.create(email, password, null);
  }

  public async authenticate(login: string, password: string): Promise<string> {
    const flow = this.getAuthenticationFlow();

    const { refreshToken } = await flow.authenticate(
      login,
      password,
      'website/user',
      null,
      { email: login }
    );

    return refreshToken;
  }

  public async getAccessToken(refreshToken: string): Promise<string> {
    const { client } = await this.getAuthenticationFlow().getClient(
      refreshToken
    );
    const token = await this.getAuthorizationFlow().getAccessToken(client);

    return token;
  }

  public async authorize(
    token: string
  ): Promise<Client<string, null, ClientMeta>> {
    const { client } = await this.getAuthorizationFlow().authorize(token);

    return client;
  }

  public async getClientsByCredentialsId(
    id: Credentials['id']
  ): Promise<Client[]> {
    const mongoDb = Registry.getMongoDb();
    const adapter = getCollectionAdapter(
      mongoDb.getDb(),
      getClientCollection(mongoDb)
    );

    return adapter.getMany({ credentialsId: id });
  }

  public getAuthenticationFlow() {
    return getAuthenticaionFlow(
      this.credentialsModel,
      this.clientModel,
      new JWT(Config.getJwtSecret())
    );
  }

  public getAuthorizationFlow() {
    return getAuthorizationFlow(
      this.clientModel,
      new JWT(Config.getJwtSecret()),
      new CSRF()
    );
  }
}

import { RegistryFactory } from '@glangeo/pollux';
import { CommonRegistry } from 'src/CommonRegistry';

export const Registry = new RegistryFactory()
  .addMethod({
    key: 'getMongoDb',
    factory: () => CommonRegistry.getMongoDb(),
  })
  .build();

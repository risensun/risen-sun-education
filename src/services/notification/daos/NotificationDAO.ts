import { MongoDB } from '@glangeo/pollux/db/drivers/mongo';
import { Notification } from 'src/common/types/notification';
import { INotificationDAO } from '../interfaces';
import { getNotificaionCollectionAdapter } from './getNotificationCollectionAdapter';

export class NotificationDAO implements INotificationDAO {
  protected readonly adapter: ReturnType<
    typeof getNotificaionCollectionAdapter
  >;

  public constructor(db: MongoDB) {
    this.adapter = getNotificaionCollectionAdapter(db);
  }

  public async create(
    data: Pick<
      Notification.Notification,
      'ownerId' | 'key' | 'title' | 'content'
    >
  ): Promise<Notification.Notification> {
    return this.adapter.create(data);
  }

  public async getById(id: number): Promise<Notification.Notification> {
    return this.adapter.getOne({ id });
  }

  public async getByOwnerId(id: number): Promise<Notification.Notification[]> {
    return this.adapter.getMany({ ownerId: id });
  }

  public async markAsRead(id: number): Promise<boolean> {
    return this.adapter.updateOne({ id }, { $set: { wasRead: true } });
  }
}

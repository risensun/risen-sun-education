import {
  createCollection,
  getCollectionAdapter,
  MongoDB,
  RecordSchema,
} from '@glangeo/pollux/db/drivers/mongo';
import { WithId } from 'mongodb';
import { Notification } from 'src/common/types/notification';

type NotificationsSettingsRecord = RecordSchema<WithId<Notification.Settings>>;

export function getNotificationsSettingsCollectionAdapter(db: MongoDB) {
  return getCollectionAdapter(
    db.getDb(),
    createCollection({
      name: 'notification__settings',

      createEntityFromDBRecord(
        record: NotificationsSettingsRecord
      ): WithId<Notification.Settings> {
        return record;
      },

      async getRecordDefaultFields() {
        return {
          createdAt: Date.now(),
        };
      },
    })
  );
}

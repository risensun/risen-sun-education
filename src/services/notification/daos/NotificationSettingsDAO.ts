import { MongoDB } from '@glangeo/pollux/db/drivers/mongo';
import { Notification } from 'src/common/types/notification';
import { INotificationSettingsDAO } from '../interfaces';
import { getNotificationsSettingsCollectionAdapter } from './getNotificationSettingsCollectionAdapter';

export class NotificationSettingsDAO implements INotificationSettingsDAO {
  protected readonly adapter: ReturnType<
    typeof getNotificationsSettingsCollectionAdapter
  >;

  public constructor(db: MongoDB) {
    this.adapter = getNotificationsSettingsCollectionAdapter(db);
  }

  public async create(
    userId: number,
    settings: Omit<Notification.Settings, 'id' | 'createdAt'>
  ): Promise<Notification.Settings> {
    return this.adapter.create({
      id: userId,
      ...settings,
    });
  }

  public async getById(
    id: Notification.Settings['id']
  ): Promise<Notification.Settings> {
    return this.adapter.getOne({ id });
  }

  public async getNotificationConfig<
    S extends keyof Notification.Settings['services'],
    D extends keyof Notification.Settings['services'][S],
    T extends keyof Notification.Settings['services'][S][D]
  >(
    id: Notification.Settings['id'],
    service: S,
    domain: D,
    type: T
  ): Promise<Notification.Config> {
    const settings = await this.getById(id);
    const config: any = settings.services[service][domain][type];

    return config as Notification.Config;
  }

  public async update(
    id: Notification.Settings['id'],
    updates: Notification.Settings['services']
  ): Promise<boolean> {
    return this.adapter.updateOne({ id }, { $set: { services: updates } });
  }
}

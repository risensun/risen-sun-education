import {
  createCollection,
  generateEntityId,
  getCollectionAdapter,
  MongoDB,
  RecordSchema,
} from '@glangeo/pollux/db/drivers/mongo';
import { WithId } from 'mongodb';
import { Notification } from 'src/common/types/notification';

type NotificationRecord = RecordSchema<WithId<Notification.Notification>>;

export function getNotificaionCollectionAdapter(db: MongoDB) {
  return getCollectionAdapter(
    db.getDb(),
    createCollection({
      name: 'notificaion__notificaions',

      createEntityFromDBRecord(
        record: NotificationRecord
      ): WithId<Notification.Notification> {
        return record;
      },

      async getRecordDefaultFields() {
        return {
          id: await generateEntityId(db.getDb(), this.name),
          wasRead: false,
          createdAt: Date.now(),
        };
      },
    })
  );
}

import { Registry } from './registry';

export class NotificationService {
  /** Aliases */
  public getNotificationsByOwnerId =
    Registry.getNotificationModel().getByOwnerId;
}

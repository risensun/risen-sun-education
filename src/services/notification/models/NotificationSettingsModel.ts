import { bindMethods } from '@glangeo/pollux/utils';
import { Notification } from 'src/common/types/notification';
import { User } from 'src/common/types/user';
import {
  INotificationSettingsDAO,
  INotificationSettingsModel,
} from '../interfaces';

@bindMethods
export class NotificationSettingsModel implements INotificationSettingsModel {
  public constructor(protected readonly dao: INotificationSettingsDAO) {}

  public async create(
    userId: User['id'],
    settings: Omit<Notification.Settings, 'id' | 'createdAt'>
  ): Promise<Notification.Settings> {
    return this.dao.create(userId, settings);
  }

  public async getById(
    id: Notification.Settings['id']
  ): Promise<Notification.Settings> {
    return this.dao.getById(id);
  }

  public async getNotificationConfig<
    S extends keyof Notification.Settings['services'],
    D extends keyof Notification.Settings['services'][S],
    T extends keyof Notification.Settings['services'][S][D]
  >(
    id: Notification.Settings['id'],
    service: S,
    domain: D,
    type: T
  ): Promise<Notification.Config> {
    return this.dao.getNotificationConfig(id, service, domain, type);
  }

  public async update(
    id: Notification.Settings['id'],
    updates: Notification.Settings['services']
  ): Promise<boolean> {
    return this.dao.update(id, updates);
  }
}

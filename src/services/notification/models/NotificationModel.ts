import { bindMethods } from '@glangeo/pollux/utils';
import { Notification } from 'src/common/types/notification';
import { INotificationDAO, INotificationModel } from '../interfaces';

@bindMethods
export class NotificationModel implements INotificationModel {
  public constructor(protected readonly dao: INotificationDAO) {}

  public async create(
    data: Pick<
      Notification.Notification,
      'ownerId' | 'key' | 'title' | 'content'
    >
  ): Promise<Notification.Notification> {
    return this.dao.create(data);
  }

  public async getByOwnerId(id: number): Promise<Notification.Notification[]> {
    return this.dao.getByOwnerId(id);
  }

  public async markAsRead(id: number): Promise<boolean> {
    return this.dao.markAsRead(id);
  }
}

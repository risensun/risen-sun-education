import { IEntityDAO } from 'src/common';
import { Notification } from 'src/common/types/notification';

export interface INotificationDAO
  extends IEntityDAO<Notification.Notification> {
  create(
    data: Pick<
      Notification.Notification,
      'ownerId' | 'key' | 'title' | 'content'
    >
  ): Promise<Notification.Notification>;

  getByOwnerId(id: number): Promise<Notification.Notification[]>;

  markAsRead(id: number): Promise<boolean>;
}

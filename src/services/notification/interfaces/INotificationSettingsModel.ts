import { Notification } from 'src/common/types/notification';
import { User } from 'src/common/types/user';

export interface INotificationSettingsModel {
  create(
    userId: User['id'],
    settings: Omit<Notification.Settings, 'id' | 'createdAt'>
  ): Promise<Notification.Settings>;

  getById(id: Notification.Settings['id']): Promise<Notification.Settings>;

  getNotificationConfig<
    S extends keyof Notification.Settings['services'],
    D extends keyof Notification.Settings['services'][S],
    T extends keyof Notification.Settings['services'][S][D]
  >(
    id: Notification.Settings['id'],
    service: S,
    domain: D,
    type: T
  ): Promise<Notification.Config>;

  update(
    id: Notification.Settings['id'],
    updates: Notification.Settings['services']
  ): Promise<boolean>;
}

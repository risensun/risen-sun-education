import { RegistryFactory } from '@glangeo/pollux';
import { CommonRegistry } from 'src/CommonRegistry';
import { NotificationDAO, NotificationSettingsDAO } from '../daos';
import { INotificationModel, INotificationSettingsDAO } from '../interfaces';
import { NotificationModel, NotificationSettingsModel } from '../models';

export const Registry = new RegistryFactory()
  .addMethod({
    key: 'getMongoDb',
    factory: () => CommonRegistry.getMongoDb(),
  })
  .addMethod({
    key: 'getNotificationModel',
    factory: (registry): INotificationModel =>
      new NotificationModel(new NotificationDAO(registry.getMongoDb())),
  })
  .addMethod({
    key: 'getNotificationSettingsModel',
    factory: (registry): INotificationSettingsDAO =>
      new NotificationSettingsModel(
        new NotificationSettingsDAO(registry.getMongoDb())
      ),
  })
  .build();

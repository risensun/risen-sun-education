import { App } from '@glangeo/pollux/api';
import {
  ServiceConstructor,
  ServiceRegistry,
} from '@glangeo/pollux/distributed/http';
import { AuthService } from './auth/AuthService';
import { HRService } from './hr/HRService';
import { NotificationService } from './notification/NotificationSevice';
import { SkillService } from './skill/SkillService';
import { UserService } from './user/UserService';

export class ServicesApp extends App {
  public services: ServiceConstructor[] = [
    AuthService,
    HRService,
    NotificationService,
    SkillService,
    UserService,
  ];

  protected async afterInit(): Promise<void> {
    super.afterInit();

    await ServiceRegistry.getService(UserService).createSuperUser();
  }
}

import { Config as PolluxConfig } from '@glangeo/pollux';

const INF_MONGO_DB_URL = 'INF_MONGO_DB_URL';
const INF_MONGO_DB_NAME = 'INF_MONGO_DB_NAME';
const INF_ADMIN_LOGIN = 'INF_ADMIN_LOGIN';
const INF_ADMIN_PASSWORD = 'INF_ADMIN_PASSWORD';
const SCRT_JWT_SECRET = 'SCRT_JWT_SECRET';

const APP_NAME = 'APP_NAME';

export abstract class Config extends PolluxConfig {
  public static getMongoDbUrl(): string {
    return this.safeGetEnvVar(INF_MONGO_DB_URL);
  }

  public static getMongoDbName(): string {
    return this.safeGetEnvVar(INF_MONGO_DB_NAME);
  }

  public static getAdminLogin(): string {
    return this.safeGetEnvVar(INF_ADMIN_LOGIN);
  }

  public static getAdminPassword(): string {
    return this.safeGetEnvVar(INF_ADMIN_PASSWORD);
  }

  public static getJwtSecret(): string {
    return this.safeGetEnvVar(SCRT_JWT_SECRET);
  }

  public static getAppName(): string {
    return this.safeGetEnvVar(APP_NAME);
  }
}

import { App as PolluxApp } from '@glangeo/pollux/api';
import cookieParser from 'cookie-parser';
import { CommonRegistry } from './CommonRegistry';

export class App extends PolluxApp {
  protected async beforeInit(): Promise<void> {
    await CommonRegistry.getMongoDb().connect();
  }

  protected async applyMiddleware(): Promise<void> {
    await super.applyMiddleware();
    this.server.use(cookieParser());
  }
}

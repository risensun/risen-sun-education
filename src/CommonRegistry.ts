import { RegistryFactory } from '@glangeo/pollux';
import { MongoDB } from '@glangeo/pollux/db/drivers/mongo';
import { Config } from './Config';

export const CommonRegistry = new RegistryFactory()
  .addMethod({
    key: 'getMongoDb',
    factory: () => new MongoDB(Config.getMongoDbUrl(), Config.getMongoDbName()),
  })
  .build();

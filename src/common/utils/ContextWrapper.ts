import { InternalException } from '@glangeo/pollux';
import { Context } from '@glangeo/pollux/api';
import { Client } from '@glangeo/pollux/plugins/auth';
import { useContext } from '../middlewares';
import { ClientMeta } from '../types/auth';

export class ContextWrapper {
  public constructor(protected readonly context: Context) {}

  public getRequiredClient(): Client<string, null, ClientMeta> {
    const state = useContext.enhancer.getState(this.context);
    const client = state.client;

    if (!client) {
      throw new InternalException({
        message: 'Could not access to client of enhanced state.',
        meta: {
          description: [
            'Possibly you forgot to add useAuthorization middleware to your request.',
            'Or it may be that authorization on this endpoint was optional.',
          ],
        },
      });
    }

    return client;
  }

  public getClient(): Client<string, null, ClientMeta> | null {
    const state = useContext.enhancer.getState(this.context);
    const client = state.client;

    return client;
  }

  public getAuthedUserEmail(): string {
    const client = this.getRequiredClient();

    return client.meta.email;
  }
}

import { CoreTypes, ValidationException } from '@glangeo/pollux';
import { getContext, HTTPStatusCode } from '@glangeo/pollux/api';
import { ServiceRegistry } from '@glangeo/pollux/distributed/http';
import { useContext } from 'src/common/middlewares/useContext';
import { AuthService } from 'src/services/auth/AuthService';

export const useAuthorization: () => CoreTypes.Api.Middleware =
  () => async (req, res, next) => {
    const token = req.cookies['authorization'];

    if (!token) {
      throw new ValidationException({
        message: 'Authorization required.',
        meta: {
          errors: [`Endpoint: ${req.route}`],
        },
        httpStatusCode: HTTPStatusCode.Unauthorized,
        publicInfo: {
          message: 'Authorization required.',
        },
      });
    }

    const client = await ServiceRegistry.getService(AuthService).authorize(
      token
    );

    const context = getContext(req, res);
    useContext.enhancer.setState(
      {
        ...useContext.enhancer.getState(context),
        client,
      },
      getContext(req, res)
    );

    next();
  };

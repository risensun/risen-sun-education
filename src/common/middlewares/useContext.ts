import {
  createContextEnhancer,
  getContext,
  setContext,
} from '@glangeo/pollux/api';
import { Middleware } from '@glangeo/pollux/core/types/api';
import { Client } from '@glangeo/pollux/plugins/auth';
import { ClientMeta } from '../types/auth';

const contextEnhancer = createContextEnhancer<{
  client: Client<string, null, ClientMeta> | null;
}>();

export function useContext(): Middleware {
  return (req, res, next) => {
    const context = getContext(req, res);

    contextEnhancer.setState({ client: null }, context);

    setContext(context, res);

    next();
  };
}

useContext.enhancer = contextEnhancer;

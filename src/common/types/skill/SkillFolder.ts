export type SkillFolder = {
  readonly name: string;
  readonly createdAt: number;
};

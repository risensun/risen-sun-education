export type Skill = {
  readonly name: string;
  readonly description: string;
  readonly attachments: {
    readonly name: string;
    readonly url: string;
  }[];
  readonly createdAt: number;
  readonly isDeleted: boolean;
};

export enum RoadmapStatus {
  NotStarted = 'NOT_STARTED',
  InProgress = 'IN_PROGRESS',
  Finished = 'FINISHED',
}

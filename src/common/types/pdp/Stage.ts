export type Stage = {
  readonly key: string;
  readonly name: string;
  readonly isPassed: boolean;
  readonly skills: {
    readonly id: number;
    readonly isPassed: boolean;
  }[];
  readonly createdAt: number;
};

import { RoadmapStatus } from './RoadmapStatus';
import { Stage } from './Stage';

export type Roadmap = {
  readonly id: number;
  readonly assigneeId: number;
  readonly reviewerId: number;
  readonly stages: Stage[];
  readonly status: RoadmapStatus;
  readonly from: {
    readonly position: string;
  };
  readonly to: {
    readonly position: string;
  };
  readonly createdAt: number;
  readonly startedAt: number | null;
};

export namespace Notification {
  export type Settings = {
    readonly id: number;
    readonly services: {
      readonly pdp: {
        readonly roadmap: {
          readonly created: Config;
          readonly passed: Config;
        };
        readonly stage: {
          readonly passed: Config;
          readonly failed: Config;
        };
      };
    };
    readonly createdAt: number;
  };

  export type Config = {
    readonly isAvailable: boolean;
    readonly transports: {
      [K in Transport.Type]: Transport.Config;
    };
  };

  export type Notification = {
    readonly id: number;
    readonly ownerId: number;
    readonly key: string;
    readonly title: string;
    readonly content: string;
    readonly wasRead: boolean;
    readonly createdAt: number;
  };

  export namespace Transport {
    export enum Type {
      Email = 'EMAIL',
      Mobile = 'MOBILE',
    }

    export type TypeToConnectionMap = {
      [Type.Email]: {
        readonly primaryEmail: string;
        readonly additionalEmails: string[];
      };
      [Type.Mobile]: {
        readonly firebaseId: string;
      };
    };

    export type Config = {
      readonly isEnabled: boolean;
    };
  }
}

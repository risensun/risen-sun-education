export type ClientMeta = {
  readonly email: string;
};

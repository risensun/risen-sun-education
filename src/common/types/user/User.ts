export type User = {
  readonly id: number;
  readonly email: string;
  readonly phone: string;
  readonly firstName: string;
  readonly lastName: string;
  readonly dateOfBirth: number;
  readonly jobTitle: string;
  readonly createdAt: number;
  readonly isActive: boolean;
};

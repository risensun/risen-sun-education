import { Currency } from './Currency';
import { Employment } from './Employpement';
import { ExperienceUnit } from './ExperienceUnit';

export type Vacancy = {
  readonly id: number;
  readonly name: string;
  readonly wage: {
    readonly from: number;
    readonly to: number;
    readonly currency: Currency;
  };
  readonly experience: {
    readonly from: number;
    readonly to: number;
    readonly unit: ExperienceUnit;
  };
  readonly employment: Employment;
  readonly requirements: string;
  readonly conditions: string;
  readonly charge: string;
  readonly candidateIds: number[];
  readonly createdAt: number;
  readonly isPublished: boolean;
  readonly isDeleted: boolean;
};

export enum CandidateStatus {
  HRInterview = 'HR_INTERVIEW',
  ProfessionalReview = 'PROFESSIONAL_REVIEW',
  Accepted = 'ACCEPTED',
  Declined = 'DECLINED',
}

export enum Employment {
  Fulltime = 'FULLTIME',
  Parttime = 'PARTTIME',
}

import { CandidateStatus } from './CandidateStatus';

export type Candidate = {
  readonly id: number;
  readonly email: string;
  readonly phone: string;
  readonly firstName: string;
  readonly lastName: string;
  readonly dateOfBirth: number;
  readonly status: CandidateStatus;
  readonly createdAt: number;
  readonly isDeleted: boolean;
};

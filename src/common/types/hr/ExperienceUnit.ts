export enum ExperienceUnit {
  Year = 'YEAR',
  Month = 'MONTH',
}

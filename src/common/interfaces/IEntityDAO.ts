export interface IEntityDAO<T extends { id: number }> {
  getById(id: T['id']): Promise<T>;
}

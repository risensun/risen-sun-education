import { RegistryFactory } from '@glangeo/pollux';
import { ServiceRegistry } from '@glangeo/pollux/distributed/http';
import { AuthService } from 'src/services/auth/AuthService';
import { HRService } from 'src/services/hr/HRService';
import { NotificationService } from 'src/services/notification/NotificationSevice';
import { PDPService } from 'src/services/pdp/PDPService';
import { SkillService } from 'src/services/skill/SkillService';
import { UserService } from 'src/services/user/UserService';

export const Registry = new RegistryFactory()
  .addMethod({
    key: 'getAuthService',
    factory: () => ServiceRegistry.getService(AuthService),
  })
  .addMethod({
    key: 'getUserService',
    factory: () => ServiceRegistry.getService(UserService),
  })
  .addMethod({
    key: 'getHRService',
    factory: () => ServiceRegistry.getService(HRService),
  })
  .addMethod({
    key: 'getSkillService',
    factory: () => ServiceRegistry.getService(SkillService),
  })
  .addMethod({
    key: 'getNotificationService',
    factory: () => ServiceRegistry.getService(NotificationService),
  })
  .addMethod({
    key: 'getPDPService',
    factory: () => ServiceRegistry.getService(PDPService),
  })
  .build();

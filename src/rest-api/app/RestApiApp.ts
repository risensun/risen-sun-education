import { App } from '@glangeo/pollux/api';
import { AuthModule } from '../modules/auth';
import { HiringModule } from '../modules/hiring';
import { PDPModule } from '../modules/pdp';
import { SkillsModule } from '../modules/skills';
import { UserModule } from '../modules/users';

export class RestApiApp extends App {
  public async enableModules(): Promise<void> {
    await this.addModule(AuthModule);
    await this.addModule(UserModule);
    await this.addModule(HiringModule);
    await this.addModule(SkillsModule);
    await this.addModule(PDPModule);
  }
}

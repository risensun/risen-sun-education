import { createEndpoint, EndpointMethod } from '@glangeo/pollux/api';
import { useAuthorization, useContext } from 'src/common/middlewares';
import { Registry } from 'src/rest-api/Registry';

export default createEndpoint({
  method: EndpointMethod.GET,
  validation: {
    query: undefined,
    params: undefined,
  },
  middlewares: [useContext(), useAuthorization()],

  action: async () => ({
    entities: await Registry.getPDPService().getAllRoadmaps(),
  }),
});

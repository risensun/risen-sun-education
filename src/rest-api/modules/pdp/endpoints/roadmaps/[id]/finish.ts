import { createEndpoint, EndpointMethod } from '@glangeo/pollux/api';
import { useAuthorization, useContext } from 'src/common/middlewares';
import { Registry } from 'src/rest-api/Registry';
import * as Yup from 'yup';

const params = Yup.object({
  id: Yup.number().positive().required(),
});

export default createEndpoint({
  method: EndpointMethod.PUT,
  validation: {
    query: undefined,
    params,
    body: undefined,
  },
  middlewares: [useContext(), useAuthorization()],

  action: async ({ params: { id } }) => ({
    isSucceeded: await Registry.getPDPService().markAsFinished(id),
  }),
});

import { createEndpoint, EndpointMethod } from '@glangeo/pollux/api';
import { useAuthorization, useContext } from 'src/common/middlewares';
import { Registry } from 'src/rest-api/Registry';
import * as Yup from 'yup';

const params = Yup.object({
  id: Yup.number().positive().required(),
});

const body = Yup.object({
  from: Yup.object({
    position: Yup.string().required(),
  }).required(),
  to: Yup.object({
    position: Yup.string().required(),
  }).required(),
});

export default createEndpoint({
  method: EndpointMethod.PUT,
  validation: {
    query: undefined,
    params,
    body,
  },
  middlewares: [useContext(), useAuthorization()],

  action: async ({ params: { id }, body: { from, to } }) => ({
    isSucceeded: await Registry.getPDPService().updateFromTo(id, { from, to }),
  }),
});

import { createEndpoint, EndpointMethod } from '@glangeo/pollux/api';
import { useAuthorization, useContext } from 'src/common/middlewares';
import { Registry } from 'src/rest-api/Registry';
import * as Yup from 'yup';

const params = Yup.object({
  id: Yup.number().positive().required(),
  key: Yup.string().required(),
});

const body = Yup.object({
  name: Yup.string().required(),
});

export default createEndpoint({
  method: EndpointMethod.PUT,
  validation: {
    params,
    body,
    query: undefined,
  },
  middlewares: [useContext(), useAuthorization()],

  action: async ({ params: { id, key }, body }) => ({
    isSucceeded: await Registry.getPDPService().renameStage(id, key, body),
  }),
});

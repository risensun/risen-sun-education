import { createEndpoint, EndpointMethod } from '@glangeo/pollux/api';
import { useAuthorization, useContext } from 'src/common/middlewares';
import { Registry } from 'src/rest-api/Registry';
import * as Yup from 'yup';

const params = Yup.object({
  id: Yup.number().positive().required(),
  key: Yup.string().required(),
});

const body = Yup.object({
  skillIds: Yup.array(Yup.number().positive().required()).required(),
  isPassed: Yup.bool().required(),
});

export default createEndpoint({
  method: EndpointMethod.PUT,
  validation: {
    query: undefined,
    params,
    body,
  },
  middlewares: [useContext(), useAuthorization()],

  action: async ({ params: { id, key }, body: { skillIds, isPassed } }) => ({
    isSucceeded: await Registry.getPDPService().setSkillsIsPassed(
      id,
      key,
      skillIds,
      isPassed
    ),
  }),
});

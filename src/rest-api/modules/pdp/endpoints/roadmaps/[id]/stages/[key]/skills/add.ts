import { createEndpoint, EndpointMethod } from '@glangeo/pollux/api';
import { useAuthorization, useContext } from 'src/common/middlewares';
import { Registry } from 'src/rest-api/Registry';
import * as Yup from 'yup';

const params = Yup.object({
  id: Yup.number().positive().required(),
  key: Yup.string().required(),
});

const body = Yup.object({
  skillId: Yup.number().positive().required(),
});

export default createEndpoint({
  method: EndpointMethod.POST,
  validation: {
    query: undefined,
    params,
    body,
  },
  middlewares: [useContext(), useAuthorization()],

  action: async ({ params: { id, key }, body: { skillId } }) => ({
    isSucceeded: await Registry.getPDPService().addSkillToStage(
      id,
      key,
      skillId
    ),
  }),
});

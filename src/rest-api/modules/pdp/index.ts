import { collectEndpoints, createModule, Router } from '@glangeo/pollux/api';

const endpoints = collectEndpoints(__dirname);

export const PDPModule = createModule({
  name: 'PDPModule',
  router: new Router({ endpoints, path: '/pdp' }),
});

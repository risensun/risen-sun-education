import { createEndpoint, EndpointMethod } from '@glangeo/pollux/api';
import { Registry } from 'src/rest-api/Registry';
import * as Yup from 'yup';

const bodySchema = Yup.object({
  token: Yup.string().required(),
});

export default createEndpoint({
  method: EndpointMethod.POST,
  validation: {
    query: undefined,
    params: undefined,
    body: bodySchema,
  },

  action: async ({ body }, req, res) => {
    const { token: refreshToken } = body;

    const accessToken = await Registry.getAuthService().getAccessToken(
      refreshToken
    );

    res.cookie('authorization', accessToken, { httpOnly: true });

    return { isSucceeded: true };
  },
});

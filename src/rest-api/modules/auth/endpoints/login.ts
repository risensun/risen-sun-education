import { createEndpoint, EndpointMethod } from '@glangeo/pollux/api';
import { Registry } from 'src/rest-api/Registry';
import * as Yup from 'yup';

const bodySchema = Yup.object({
  login: Yup.string().required().email(),
  password: Yup.string().required(),
});

export default createEndpoint({
  method: EndpointMethod.POST,
  validation: {
    query: undefined,
    params: undefined,
    body: bodySchema,
  },

  action: async ({ body }) => {
    const { login, password } = body;
    const refreshToken = await Registry.getAuthService().authenticate(
      login,
      password
    );

    return { token: refreshToken };
  },
});

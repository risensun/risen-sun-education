import {
  createEndpoint,
  EndpointMethod,
  getContext,
} from '@glangeo/pollux/api';
import { useContext, useAuthorization } from 'src/common/middlewares';
import { ContextWrapper } from 'src/common/utils';
import { Registry } from 'src/rest-api/Registry';

export default createEndpoint({
  method: EndpointMethod.GET,
  middlewares: [useContext(), useAuthorization()],

  action: async (_, req, res) => {
    const context = new ContextWrapper(getContext(req, res));
    const client = context.getRequiredClient();

    const clients = await Registry.getAuthService().getClientsByCredentialsId(
      client.credentialsId
    );

    return { entities: clients };
  },
});

import { collectEndpoints, createModule, Router } from '@glangeo/pollux/api';

const endpoints = collectEndpoints(__dirname);

export const AuthModule = createModule({
  name: 'AuthModule',
  router: new Router({ endpoints, path: '/auth' }),
});

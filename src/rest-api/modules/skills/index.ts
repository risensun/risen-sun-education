import { collectEndpoints, createModule, Router } from '@glangeo/pollux/api';

const endpoints = collectEndpoints(__dirname);

export const SkillsModule = createModule({
  name: 'SkillsModule',
  router: new Router({ endpoints, path: '/skills' }),
});

import { createEndpoint, EndpointMethod } from '@glangeo/pollux/api';
import { useAuthorization, useContext } from 'src/common/middlewares';
import { Registry } from 'src/rest-api/Registry';
import * as Yup from 'yup';

const body = Yup.object({
  folderId: Yup.number().positive().nullable().default(null),
  skill: Yup.object({
    name: Yup.string().required(),
    description: Yup.string().required(),
    attachments: Yup.array(
      Yup.object({
        name: Yup.string().required(),
        url: Yup.string().url().required(),
      }).required()
    ).required(),
  }).required(),
});

export default createEndpoint({
  method: EndpointMethod.POST,
  validation: {
    query: undefined,
    params: undefined,
    body,
  },
  middlewares: [useContext(), useAuthorization()],

  action: async ({ body: { folderId, skill } }) =>
    Registry.getSkillService().createSkill(skill, folderId),
});

import { createEndpoint, EndpointMethod } from '@glangeo/pollux/api';
import { useContext, useAuthorization } from 'src/common/middlewares';
import { Registry } from 'src/rest-api/Registry';
import * as Yup from 'yup';

const params = Yup.object({ id: Yup.number().positive().required() });
const body = Yup.object({
  name: Yup.string().required(),
  description: Yup.string().required(),
  attachments: Yup.array(
    Yup.object({
      name: Yup.string().required(),
      url: Yup.string().url().required(),
    }).required()
  ).required(),
});

export default createEndpoint({
  method: EndpointMethod.PUT,
  validation: {
    query: undefined,
    params,
    body,
  },
  middlewares: [useContext(), useAuthorization()],
  action: async ({
    params: { id },
    body: { name, description, attachments },
  }) => ({
    isSucceeded: await Registry.getSkillService().updateSkill(id, {
      name,
      description,
      attachments,
    }),
  }),
});

import { createEndpoint, EndpointMethod } from '@glangeo/pollux/api';
import { useAuthorization, useContext } from 'src/common/middlewares';
import { Registry } from 'src/rest-api/Registry';
import * as Yup from 'yup';

const params = Yup.object({
  id: Yup.number().positive().required(),
});

export default createEndpoint({
  method: EndpointMethod.GET,
  validation: {
    query: undefined,
    params,
  },
  middlewares: [useContext(), useAuthorization()],

  action: async ({ params: { id } }) => ({
    entities: await Registry.getSkillService().getBreadcrumbs(id),
  }),
});

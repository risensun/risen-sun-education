import { createEndpoint, EndpointMethod } from '@glangeo/pollux/api';
import { useContext, useAuthorization } from 'src/common/middlewares';
import { Registry } from 'src/rest-api/Registry';
import * as Yup from 'yup';

const params = Yup.object({ id: Yup.number().positive().required() });
const body = Yup.object({
  parentId: Yup.number().positive().optional().default(null),
});

export default createEndpoint({
  method: EndpointMethod.PUT,
  validation: {
    params,
    body,
    query: undefined,
  },
  middlewares: [useContext(), useAuthorization()],

  action: async ({ params: { id }, body: { parentId } }) => ({
    isSucceeded: await Registry.getSkillService().moveToFolderOrRoot(
      id,
      parentId
    ),
  }),
});

import { createEndpoint, EndpointMethod } from '@glangeo/pollux/api';
import { useContext, useAuthorization } from 'src/common/middlewares';
import { Registry } from 'src/rest-api/Registry';

export default createEndpoint({
  method: EndpointMethod.GET,
  // TODO: think how to prevent importing middleware from other service
  middlewares: [useContext(), useAuthorization()],

  action: async () => ({
    entities: await Registry.getSkillService().getSkills(),
  }),
});

import { collectEndpoints, createModule, Router } from '@glangeo/pollux/api';

const endpoints = collectEndpoints(__dirname);

export const HiringModule = createModule({
  name: 'HiringModule',
  router: new Router({ endpoints, path: '/hiring' }),
});

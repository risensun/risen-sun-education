import { createEndpoint, EndpointMethod } from '@glangeo/pollux/api';
import { useContext, useAuthorization } from 'src/common/middlewares';
import { Currency, Employment, ExperienceUnit } from 'src/common/types/hr';
import { Registry } from 'src/rest-api/Registry';
import * as Yup from 'yup';

const bodySchema = Yup.object({
  name: Yup.string().required(),
  wage: Yup.object({
    from: Yup.number().positive().required(),
    to: Yup.number().positive().required(),
    currency: Yup.string().equals(Object.values(Currency)).required(),
  }).required(),
  experience: Yup.object({
    from: Yup.number().positive().required(),
    to: Yup.number().positive().required(),
    unit: Yup.string().equals(Object.values(ExperienceUnit)).required(),
  }).required(),
  employment: Yup.string().equals(Object.values(Employment)).required(),
  requirements: Yup.string().required(),
  conditions: Yup.string().required(),
  charge: Yup.string().required(),
});

export default createEndpoint({
  method: EndpointMethod.POST,
  validation: {
    query: undefined,
    params: undefined,
    body: bodySchema,
  },
  middlewares: [useContext(), useAuthorization()],
  action: async ({ body }) => ({
    entity: await Registry.getHRService().createVacancy(body),
  }),
});

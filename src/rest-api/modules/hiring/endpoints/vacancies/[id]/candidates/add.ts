import { createEndpoint, EndpointMethod } from '@glangeo/pollux/api';
import { useContext, useAuthorization } from 'src/common/middlewares';
import { Registry } from 'src/rest-api/Registry';
import * as Yup from 'yup';

const paramsSchema = Yup.object({
  id: Yup.number().positive().required(),
});

const bodySchema = Yup.object({
  candidateId: Yup.number().positive().required(),
});

export default createEndpoint({
  method: EndpointMethod.PUT,
  validation: {
    query: undefined,
    params: paramsSchema,
    body: bodySchema,
  },
  middlewares: [useContext(), useAuthorization()],

  action: async ({ params: { id }, body: { candidateId } }) => ({
    isSucceeded: await Registry.getHRService().addCandidateToVacancy(
      id,
      candidateId
    ),
  }),
});

import * as Yup from 'yup';
import { createEndpoint, EndpointMethod } from '@glangeo/pollux/api';
import { useContext, useAuthorization } from 'src/common/middlewares';
import { Registry } from 'src/rest-api/Registry';

const querySchema = Yup.object({
  published: Yup.bool().default(false),
});

export default createEndpoint({
  method: EndpointMethod.GET,
  validation: {
    query: querySchema,
    params: undefined,
  },
  middlewares: [useContext(), useAuthorization()],

  action: async ({ query: { published } }) => ({
    entities: await Registry.getHRService().getVacancies(published),
  }),
});

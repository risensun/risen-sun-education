import * as Yup from 'yup';
import { createEndpoint, EndpointMethod } from '@glangeo/pollux/api';
import { useContext, useAuthorization } from 'src/common/middlewares';
import { Registry } from 'src/rest-api/Registry';

const querySchema = Yup.object({
  current: Yup.bool().default(false),
});

export default createEndpoint({
  method: EndpointMethod.GET,
  validation: {
    query: querySchema,
    params: undefined,
  },
  middlewares: [useContext(), useAuthorization()],

  action: async ({ query: { current } }) => {
    const candidates = await Registry.getHRService().getCandidates(current);

    return {
      entities: candidates,
    };
  },
});

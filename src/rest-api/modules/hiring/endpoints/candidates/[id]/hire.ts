import { createEndpoint, EndpointMethod } from '@glangeo/pollux/api';
import { useContext, useAuthorization } from 'src/common/middlewares';
import { Registry } from 'src/rest-api/Registry';
import * as Yup from 'yup';

const paramsSchema = Yup.object({
  id: Yup.number().positive().required(),
});

const bodySchema = Yup.object({
  jobTitle: Yup.string().required(),
});

export default createEndpoint({
  method: EndpointMethod.POST,
  validation: {
    query: undefined,
    params: paramsSchema,
    body: bodySchema,
  },
  middlewares: [useContext(), useAuthorization()],

  action: async ({ params: { id }, body: { jobTitle } }) => {
    const user = await Registry.getHRService().hireCandidate(id, jobTitle);

    return { entity: user };
  },
});

import { createEndpoint, EndpointMethod } from '@glangeo/pollux/api';
import { useContext, useAuthorization } from 'src/common/middlewares';
import { Registry } from 'src/rest-api/Registry';
import * as Yup from 'yup';

const paramsSchema = Yup.object({
  id: Yup.number().positive().required(),
});

export default createEndpoint({
  method: EndpointMethod.GET,
  validation: {
    query: undefined,
    params: paramsSchema,
  },
  middlewares: [useContext(), useAuthorization()],

  action: async ({ params: { id } }) => {
    const candidate = await Registry.getHRService().getCandidateById(id);

    return { entity: candidate };
  },
});

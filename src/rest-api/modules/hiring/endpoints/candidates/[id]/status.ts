import { createEndpoint, EndpointMethod } from '@glangeo/pollux/api';
import { useContext, useAuthorization } from 'src/common/middlewares';
import { CandidateStatus } from 'src/common/types/hr';
import { Registry } from 'src/rest-api/Registry';
import * as Yup from 'yup';

const paramsSchema = Yup.object({
  id: Yup.number().positive().required(),
});

const bodySchema = Yup.object({
  status: Yup.string().equals(Object.values(CandidateStatus)).required(),
});

export default createEndpoint({
  method: EndpointMethod.PUT,
  validation: {
    query: undefined,
    params: paramsSchema,
    body: bodySchema,
  },
  middlewares: [useContext(), useAuthorization()],

  action: async ({ params: { id }, body: { status } }) => {
    const isSucceeded = await Registry.getHRService().updateCandidateStatus(
      id,
      status
    );

    return { isSucceeded };
  },
});

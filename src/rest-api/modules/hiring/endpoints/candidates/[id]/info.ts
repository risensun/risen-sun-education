import { createEndpoint, EndpointMethod } from '@glangeo/pollux/api';
import { useContext, useAuthorization } from 'src/common/middlewares';
import { Registry } from 'src/rest-api/Registry';
import * as Yup from 'yup';

const paramsSchema = Yup.object({
  id: Yup.number().positive().required(),
});

const bodySchema = Yup.object({
  email: Yup.string().email().required(),
  firstName: Yup.string().required(),
  lastName: Yup.string().required(),
  phone: Yup.string().required(),
  dateOfBirth: Yup.number().positive().required(),
});

export default createEndpoint({
  method: EndpointMethod.PUT,
  validation: {
    query: undefined,
    params: paramsSchema,
    body: bodySchema,
  },
  middlewares: [useContext(), useAuthorization()],

  action: async ({ params: { id }, body }) => {
    const isSucceeded = await Registry.getHRService().updateCandidate(id, body);

    return { isSucceeded };
  },
});

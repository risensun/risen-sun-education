import { createEndpoint, EndpointMethod } from '@glangeo/pollux/api';
import { Registry } from 'src/rest-api/Registry';

import * as Yup from 'yup';

const paramsSchema = Yup.object({
  id: Yup.number().positive().required(),
});

export default createEndpoint({
  method: EndpointMethod.GET,
  validation: {
    query: undefined,
    params: paramsSchema,
  },

  action: async ({ params: { id } }) => ({
    entity: await Registry.getUserService().getUserById(id),
  }),
});

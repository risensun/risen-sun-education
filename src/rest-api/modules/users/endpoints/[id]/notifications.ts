import * as Yup from 'yup';
import { createEndpoint, EndpointMethod } from '@glangeo/pollux/api';
import { useContext, useAuthorization } from 'src/common/middlewares';
import { Registry } from 'src/rest-api/Registry';

const params = Yup.object({ id: Yup.number().positive().required() });

export default createEndpoint({
  method: EndpointMethod.GET,
  validation: {
    params,
    query: undefined,
  },
  middlewares: [useContext(), useAuthorization()],

  action: async ({ params: { id } }) => ({
    entities: await Registry.getNotificationService().getNotificationsByOwnerId(
      id
    ),
  }),
});

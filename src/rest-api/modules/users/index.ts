import { collectEndpoints, createModule, Router } from '@glangeo/pollux/api';

const endpoints = collectEndpoints(__dirname);

export const UserModule = createModule({
  name: 'UserModule',
  router: new Router({ endpoints, path: '/users' }),
});
